# ansible-scripts


## Getting started

This repo project consists of different roles used to automate the installation of core dependencies on a fresh server. 

## Run Playbook
To run the playbook configure your hosts file with corresponding hosts

```

[server]  # this is a test server
ip-addr  ansible_ssh_port=port  ansible_ssh_user=user

```
Run the Playbook with the command below
```
ansible-playbook -i hosts playbook.yml --ask-become-pass -vvv

```
